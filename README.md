# team39

Discontinued features proposal:

Admin:
N/A

User-Home: 
	User will be able to rename a file.
		=> 	User can just delete and re-upload with a different name
	User will connect their cloud repositories
		=> 	Only a select few cloud storage/repositories can be added. 
			There is not enough time to provide support for all cloud storage/site. 
			We will only support Google Drive and Drop Box 
	User home will only have 2 divisions (one for side nav and the other for main overlay)
		=> 	We won't create another div for Upload. (Refer the Diagram for User - View in Proposal).
			Adding/Uploading files will be displayed as a button in the file-overlay instead
		=>	We are no longer have share file/ create file option. 
Profile:
N/A
