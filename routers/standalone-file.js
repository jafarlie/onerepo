/* file database functions */

'use strict'
const log = console.log
const print = console.log

const express = require('express')
const port = process.env.PORT || 3000
const bodyParser = require('body-parser')
const mongo = require('mongodb')
//const mongoose = require('mongoose')
const { mongoose } = require('../db/mongoose')
const path = require('path')
const multer = require('multer')
const gridFsStorage = require('multer-gridfs-storage')
const gridStream = require('gridfs-stream')
const methodOverride = require('method-override')
gridStream.mongo = mongoose.mongo
const conn = mongoose.connection
var assert = require('assert');
var fs = require('fs');
var {mongodb, ObjectID} = require('mongodb');
const { User } = require('../src/models/user');


const datetime = require('date-and-time') 

// import the model
const { File } = require('../models/file')

// express
const router = express.Router();
// body-parser middleware - will parse the JSON and convert to object
//router.use(bodyParser.json())
//router.use(methodOverride('_method'))      




var storage = gridFsStorage({
        url: "mongodb+srv://team39:team39@team39-lbiwu.mongodb.net/team39?retryWrites=true",
        file: (req, file)=>{ 

                
                const date = new Date()
                const dateString = datetime.format(date, "MMM D, YYYY hh:mm A")
                const name = file.originalname
                return {
                metadata: {
                filename: name,
                parentID: req.params.parentID,
                date_modified: dateString,
                created_by: req.session.user
                }
        }
        }

})

var upload = multer({
        storage: storage
}).single('file')

/* Schema
|_id|parent_ID|f_SIZE|f_TIMESTAMP|f_NAME|f_PATH|
*/

/*POST Tests
Passing Test
Comments : parent_ID must be 12 byte 'char' or 24 byte 'hex'
{
        "parent_ID" : "aaaaaaaaaaaaaaaaaaaaaaaa",
        "f_NAME" : "Text.txt,
}

Failing Test
{
	"parent_ID" : "aaaaaaaaaaaaaaaaaaaaaa",
        "f_NAME" : "Text.txt"
}
*/

const authenticate = (req, res, next) => {
	if (req.session.user) {
		User.findById(req.session.user).then((user) => {
			if (!user) {
				return Promise.reject();
			} else {
				req.user = user;
				next();
			}
		}).catch((error) => {
			res.redirect('/login');
		});
	} else {
		res.redirect('/login');
	}
};

router.post('/file/:parentID', authenticate, (req,res) => {
        console.log('uploading')
	print(req)
        upload(req, res, (err) =>{
                const id = req.body.id
                if(err){
                        console.log(err)

                        res.status(500).send("could not POST")

                }
                else{
                        res.status(204).send();
                }

        })
})


router.get('/files', authenticate, (req,res) => {
        File.find({}).then((files) => {
                res.status(200).send(files)
        }).catch((error) => {
                res.status(400).send({error : error.message});
        })
})

/*
Return meta data
*/
router.get('/file/:id', authenticate, (req,res) => {

        conn.db.collection('fs.files').findOne({ "_id": new ObjectID(req.params.id)}, function (err, file) {
    if (err) {
        return res.status(400).send(err);
    }
    else if (!file) {

        return res.status(404).send('could not find file in db');
    }
    const size = Math.round(file.length/1024)
        
        const sizeString = parseInt(size) + " KB"


    const response = {
        filename: file.metadata.filename,
        parentID: file.metadata.parentID,
        date_modified: file.metadata.date_modified,
        size: sizeString,
        created_by: file.metadata.created_by
    }

    res.send(response)

})

})

/*
Returns actual file give the id 
*/
router.get('/fileDownload/:id', authenticate ,(req,res) => {
        conn.db.collection('fs.files').findOne({ "_id": new ObjectID(req.params.id)}, function (err, file) {
    if (err) {
        return res.status(400).send(err);
    }
    else if (!file) {
        return res.status(404).send('Error on the database looking for the file.');
    }
    const size = Math.round(file.length/1024)
        
        const sizeString = parseInt(size) + " KB"


    const response = {
        filename: file.metadata.filename,
        parentID: file.metadata.parentID,
        date_modified: file.metadata.date_modified,
        size: sizeString
    }





    var bucket = new mongo.GridFSBucket(conn.db, {
            chunkSizeBytes: 261120,
            bucketName: 'fs.chunks'
        });


    var responsedata = bucket.openDownloadStreamByName(file.filename)
        
        conn.db.collection('fs.chunks').find({"files_id": file._id}, (err, chunkCursor) =>{

                if(err){
                        res.status(500).send()
                }
                else{
                    

                    chunkCursor.toArray((err, arr) =>{

                        if (err){

                            console.log(err)
                        }

                        var finalArray = []

                        for (let i = 0; i < arr.length; i++){


                            finalArray.push(arr[i].data.buffer)


                        }


                        res.set('Content-Type', file.contentType)
                        res.set('Content-Disposition', "inline; filename=" + file.metadata.filename);

                        res.attachment(file.metadata.filename)
                        res.send(Buffer.concat(finalArray))

                    })


                }

        })

  });   


})


/*
Pre: Give parent_ID for file
Return : Meta data like |file_id|parent_ID|f_SIZE|f_TIMESTAMP|f_NAME|
*/
router.get('/filesByParentID/:id', authenticate, (req,res) => {
        
        conn.db.collection('fs.files').find({ "metadata.parentID": req.params.id}).toArray(function(err, result) {
    if (err) throw err;

    var finalArray = []


    for (let i = 0; i < result.length; i++) {
        var size1 = Math.round(result[i].length/1024)
        
                var sizeString = parseInt(size1) + " KB"

        var fileJSON = {
                id: result[i]._id,
                filename: result[i].metadata.filename,
                parentID: result[i].metadata.parentID,
                date_modified: result[i].metadata.date_modified,
                created_by: result[i].metadata.created_by,
                size: sizeString

        }
        finalArray.push(fileJSON)
    }



    res.send(finalArray);
  });

})

router.patch('/file/:id', authenticate, (req,res) => {
        const _id = req.params.id

        const updates = Object.keys(req.body);
        const allowedUpdates = ['d_NAME'];
        const isValidOperation = updates.every((update) => {
                return allowedUpdates.includes(update);
        })

        if(!isValidOperation){
                return res.status(400).send({error : "Invalid update keys"});
        }

        Directory.findByIdAndUpdate( 	req.params.id,
                                	req.body,
                                	{new : true, runValidators: true}
        ).then((directory) => {
                if(!directory){
                        return res.status(400).send({"error" : "Directory not found"});
                }
                res.send(task);
        }).catch((error) => {
                res.status(400).send({"error" : error.message});
        })

})

router.delete('/file/:id', authenticate, (req,res) => {
        const id = req.params.id

        // Good practise: Validate the id
        if (!ObjectID.isValid(id)) {
                res.status(404).send()
        }

        conn.db.collection('fs.files').deleteOne({_id: ObjectID(id)}).then((file) => {
                if (!file) {
                        res.status(404).send()
                } else {
                        res.send(file)
                }
        }).catch((error) => {
                res.status(500).send()
        })
})




module.exports = router

