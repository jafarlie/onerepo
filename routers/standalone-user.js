/* user database functions */
'use strict'
const log = console.log
const print = console.log

const express = require('express')
const router = new express.Router();
const {User} = require('../models/user.js');
const { MongoClient, ObjectID } = require('mongodb')

/* Schema
|_id|name|email|password|is_admin|
*/

/// User routes below
app.post('/user', (req, res) => {

	const user = new User({
		name: req.body.name,
		email: req.body.email,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	})

	// save user to the database
	user.save().then((user) => {
		res.send(user)
	}, (error) => {
		res.status(400).send(error) // 400 for bad request
	})


})

app.get('/users/:email', (req, res) => {
	// parameters from the url
	//log(req.params)
	const email = req.params.email


	// Otherwise, findByEmail
	User.findOne({ "email": email}).then((user) => {
		if (!user) {
			res.status(404).send()
		} else {
			/// sometimes will wrap object in object
			/// to specify what the object is
			//res.send({ user })
			res.send(user)

		}
	}).catch((error) => {
		res.status(500).send()
	})
})


app.post('/users/:id', (req, res) => {
	const id = req.params.id

	// grab the properties we want to change
	const { name, email, password, isAdmin } = req.body
	const body = { name, email, password, isAdmin }

	// Good practise: Validate the id
	if (!ObjectID.isValid(id)) {
		res.status(404).send()
	}

	//patch it
	User.findByIdAndUpdate(id, {$set: body}, {new: true}).then((user) => {
		if (!user) {
			res.status(404).send()
		} else {
			res.send(user)
		}
	}).catch((error) => {
		res.status(500).send()
	})
})

app.delete('/users/:id', (req, res) => {
	const id = req.params.id

	// Good practise: Validate the id
	if (!ObjectID.isValid(id)) {
		res.status(404).send()
	}

	User.findByIdAndRemove(id).then((user) => {
		if (!user) {
			res.status(404).send()
		} else {
			res.send(user)
		}
	}).catch((error) => {
		res.status(500).send()
	})
})




app.listen(port, () => {
	log(`Listening on port ${port}...`)
}) 
