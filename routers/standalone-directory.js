const express = require('express');
const router = new express.Router();
const {Directory} = require('../models/directory.js');
const {MongoClient, ObjectID} = require('mongodb');
const { User } = require('../src/models/user');
const print = console.log;

/* Schema
|_id|parent_ID|d_TIMESTAMP|d_NAME|d_ROOT|
*/

/*POST Tests
Passing Test
Comments : parent_ID must be 12 byte 'char' or 24 byte 'hex'
{
	"d_NAME" : "Google Drive",
	"d_ROOT" : true,
}
{
        "parent_ID" : "aaaaaaaaaaaaaaaaaaaaaaaa",
        "d_NAME" : "Google Drive",
        "d_ROOT" : false
}

Failing Test
{
	"parent_ID" : "abcdefGHIJKLZZZ"
        "d_NAME" : "Google Drive"
        "d_ROOT" : false,

}
*/

const authenticate = (req, res, next) => {
	if (req.session.user) {
		User.findById(req.session.user).then((user) => {
			if (!user) {
				return Promise.reject();
			} else {
				req.user = user;
				next();
			}
		}).catch((error) => {
			res.redirect('/login');
		});
	} else {
		res.redirect('/login');
	}
};

router.post('/directory', authenticate, (req,res) => {
	
	req.body.parent_ID = new ObjectID(req.body.parent_ID);
	req.body["created_BY"] = new ObjectID(req.session.user); 
        const dir = new Directory(req.body);

        dir.save().then((result) => {
                res.send(result);
        }).catch((error) =>{
                res.status(400).send({error : error.message});
        });
})

router.get('/directories', authenticate ,(req,res) => {
        Directory.find({}).then((directories) => {
                res.status(200).send(directories)
        }).catch((error) => {
                res.status(400).send({error : error.message});
        })
})

router.get('/directory/:id', authenticate, (req,res) => {
        const id = req.params.id;
        Directory.findById(id).then((directory) => {
                if(!directory){
                        return res.status(400).send({error : "`directory` not found"});
                	print('Here1')
		}
                res.status(200).send(directory);
        }).catch((error) => {
        	print('Here2')
	        res.status(400).send({error : error.message});
        })

})

router.get('/rootDirectories', authenticate , (req,res) => {
	Directory.find({d_ROOT: true}).then((directories) => {
                //print(directories);
		res.send(directories);
	}).catch((error) => {
		res.status(400).send({error : "Error fetching root directories."});
	})
})

router.get('/directoriesByParentID/:id', authenticate,(req,res) => {
        const id = req.params.id;
        Directory.find({parent_ID : new ObjectID(id), created_BY : new ObjectID(req.session.user)}).then((directories) => {
                if(!directories){
              		return res.status(400).send({error: "`directory` not found"});
                }
		print('Send Directories');
		print(directories);
                res.status(200).send(directories);
        }).catch((error) => {
                res.status(400).send({error : error.message});
        })
})


router.patch('/directory/:id', authenticate,(req,res) => {
        const _id = req.params.id

        const updates = Object.keys(req.body);
        const allowedUpdates = ['d_NAME', 'created_BY'];
        const isValidOperation = updates.every((update) => {
                return allowedUpdates.includes(update);
        })

        if(!isValidOperation){
                return res.status(400).send({error : "Invalid update keys"});
        }

        Directory.findByIdAndUpdate( 	req.params.id,
                                	req.body,
                                	{new : true, runValidators: true}
        ).then((directory) => {
                if(!directory){
                        return res.status(400).send({"error" : "Directory not found"});
                }
                res.send(directory);
        }).catch((error) => {
                res.status(400).send({"error" : error.message});
        })

})

router.delete('/directory/:id', authenticate, (req,res) => {
        const _id = req.params.id

        console.log(_id)

	//[To Do] Have to recursively delete sub directories and files 

        Directory.findByIdAndDelete(req.params.id).then((directory) => {
                /*
                if(!directory){
                        res.status(404).send({"error" : "no such directory to delete"});
                }
                */
                res.send(directory);
        }).catch((error) => {

                res.status(400).send({"error" : error.message});
        })
})

module.exports = router

