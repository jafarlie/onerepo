const express = require('express');
const router = new express.Router();
const {UserDrive} = require('../models/user_drive.js');
const {MongoClient, ObjectID} = require('mongodb');

/* Schema
|_id|u_ID|repo_ROOT|is_AUTH|auth_TOKEN|
*/

/*POST Tests
Passing Test
Comments : parent_ID must be 12 byte 'char' or 24 byte 'hex'
{
	"d_NAME" : "Google Drive",
	"d_ROOT" : true,
}
{
        "parent_ID" : "aaaaaaaaaaaaaaaaaaaaaaaa",
        "d_NAME" : "Google Drive",
        "d_ROOT" : false
}

Failing Test
{
	"parent_ID" : "abcdefGHIJKLZZZ"
        "d_NAME" : "Google Drive"
        "d_ROOT" : false,

}
*/
router.post('/userdrive', (req,res) => {
	if(req.body.d_ROOT){
		req.body.parent_ID = new ObjectID('000000000000000000000000'); //Hard Coded Universal Root Id
	}else{
		req.body.parent_ID = new ObjectID(req.body.parent_ID);
	}
        const dir = new UserDrive(req.body);

        dir.save().then((result) => {
                res.send(result);
        }).catch((error) =>{
                res.status(400).send({error : error.message});
        });
})

router.get('/userdrive', (req,res) => {
        UserDrive.find({}).then((tasks) => {
                res.status(200).send(tasks)
        }).catch((error) => {
                res.status(400).send({error : error.message});
        })
})

router.get('/userdriveUid/:id', (req,res) => {
        const id = req.params.id;
        UserDrive.findById(id).then((task) => {
                if(!directory){
                        return res.status(400).send({error : "`directory` not found"});
                }
                res.status(200).send(directory);
        }).catch((error) => {
                res.status(400).send({error : error.message});
        })

})


router.patch('/userdrive/:id', (req,res) => {
        const _id = req.params.id

        const updates = Object.keys(req.body);
        const allowedUpdates = ['d_NAME'];
        const isValidOperation = updates.every((update) => {
                return allowedUpdates.includes(update);
        })

        if(!isValidOperation){
                return res.status(400).send({error : "Invalid update keys"});
        }

        UserDrive.findByIdAndUpdate( 	req.params.id,
                                	req.body,
                                	{new : true, runValidators: true}
        ).then((directory) => {
                if(!directory){
                        return res.status(400).send({"error" : "UserDrive not found"});
                }
                res.send(task);
        }).catch((error) => {
                res.status(400).send({"error" : error.message});
        })

})

router.delete('/userdrive/:id', (req,res) => {
        const _id = req.params.id

	//[To Do] Have to recursively delete sub directories and files 

        UserDrive.findByIdAndDelete(req.params.id).then((task) => {
                if(!directory){
                        return res.status(404).send({"error" : "no such directory to delete"});
                }
                res.send(directory);
        }).catch((error) => {
                res.status(400).send({"error" : error.message});
        })
})

module.exports = router

