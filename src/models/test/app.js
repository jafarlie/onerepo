/* Testing Models on CRUD (Create,Recieve,Update,Delete) */

const print = console.log;

/* TESTING DIRECTORY MODEL BEGIN */

//Webserver
const express = require('express');
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

//Mongodb
const mongoose = require('../../db/mongoose-fs.js');
const {Directory, File} = require('../fs.js');
const {MongoClient, ObjectID} = require('mongodb');

/* Directory : d_ID | p_NODE | d_TIMESTAMP | d_NAME */

//CREATE
/*
{
	"P_DIR" : null,
	"D_NAME" : "Ben Dover"
	"D_ROOT" : "true"
}
*/
app.post('/students', (req, res) => {

	if(!(req.body.D_ROOT) || !(req.body.P_DIR)){
		return res.status(420).send('Must provide p_NODE if director is not a root directory');
	}	

	//Create a new directory
	const directory = new Directory({
		d_ID : new ObjectID(),
		p_NODE : (req.body.P_DIR)?(new ObjectID(req.body.P_DIR)):new ObjectID(),
		d_NAME : req.body.D_NAME,
		d_ROOT : req.body.D_ROOT
	});

	directory.save().then((result) => {
		res.send(result);
	}).catch((error) => {
		res.status(420).send(error);
	});

})

//RECIEVE - Sends back List of Files and Directories in the current directory



app.listen(port, () => {
	print(`Listening on port ${port}...`);
});

/* TESTING DIRECTORY MODEL END */
