/* File Explorer DB Models */

const print = console.log;
const mongoose = require('mongoose');
const validator = require('validator');
const Schema = mongoose.Schema;
const {MongoClient, ObjectID} = require('mongodb');

/* Directory Model START */

const Directory = mongoose.model('Directorie', {
	d_ID : { // Set by backend
		type : Schema.Types.ObjectId,
		required : true,
		unique : true,
	},
	p_NODE : { // Set by User. [HTML request]
		type : Schema.Types.ObjectId,
		validate : function (value) { //Custom Validation
                        if(!ObjectID.isValid(value)){
                                throw new Error(`Parent Directory ${value} is Invalid`);
                        }
			print(this);
                },
	},
	d_TIMESTAMP : { // Set by Mongoose Validator
		type : Date,
		default : Date.now,
	},
	d_NAME : { // Set by User. Need to set up a HTTP Endpoint for User to enter data
		type : String,
		required : true,
		trim : true,
		minlength : [5 , 'Directory name has to be at least greater than 5 characters'],
	},
	d_ROOT : { // Set by User.
		type : Boolean,
		required : true,
		default : false,
	} 

})

/* Directory Model END */

/* File Model START */

const File = null;

/* File Model END */

module.exports = {
	Directory : Directory,
	File : File,
}
