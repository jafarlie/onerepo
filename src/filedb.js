/* file database functions */


'use strict'
const log = console.log

const express = require('express')
const port = process.env.PORT || 3000
const bodyParser = require('body-parser')
const { ObjectID } = require('mongodb')
//const mongoose = require('mongoose')
const { mongoose } = require('./db/mongoose-file')
const path = require('path')
const multer = require('multer')
const gridFsStorage = require('multer-gridfs-storage')
const gridStream = require('gridfs-stream')
const methodOverride = require('method-override')
gridStream.mongo = mongoose.mongo
const conn = mongoose.connection

const gfs = gridStream("FileAPI", gridStream.mongo)

// import the model
const { File } = require('./models/file')

// express
const app = express();
// body-parser middleware - will parse the JSON and convert to object
app.use(bodyParser.json())
app.use(methodOverride('_method'))	



var storage = gridFsStorage({
	url: process.env.MongoURI || 'mongodb://localhost:27017/FileAPI',
	gfs: gfs,
	file: (req, file)=>{ 

		const name = file.originalname
		console.log(name)
		return {
		metadata: {
		filename: name
	}
	}
	}

})

var upload = multer({
	storage: storage
}).single('file')


app.post('/upload', (req, res) =>{

	upload(req, res, (err) =>{

		if(err){
			console.log("could not post")

			res.status(500).send("could not POST")

		}
		else{


			res.send(`Posted`)
		}

	})


})



/// File routes below
app.post('/files', (req, res) => {

	const file = new File({
		name: req.body.name,
		size: req.body.size,
		modified: req.body.modified,
		parentID: req.body.parentID,
		contentID: req.body.contentID
	})

	// save file to the database
	file.save().then((file) => {
		res.send(file)
	}, (error) => {
		res.status(400).send(error) // 400 for bad request
	})


})

app.get('/files/:id', (req, res) => {
	// parameters from the url
	//log(req.params)
	const id = req.params.id


	// Otherwise, findByEmail
	File.findOne({ "_id": id}).then((file) => {
		if (!file) {
			res.status(404).send()
		} else {
			/// sometimes will wrap object in object
			/// to specify what the object is
			//res.send({ file })
			res.send(file)

		}
	}).catch((error) => {
		res.status(500).send()
	})
})


app.patch('/files/:id', (req, res) => {
	const id = req.params.id

	// grab the properties we want to change
	const { name, size, modified, parentID, contentID } = req.body
	const body = { name, size, modified, parentID, contentID }

	// Good practise: Validate the id
	if (!ObjectID.isValid(id)) {
		res.status(404).send()
	}

	//patch it
	File.findByIdAndUpdate(id, {$set: body}, {new: true}).then((file) => {
		if (!file) {
			res.status(404).send()
		} else {
			res.send(file)
		}
	}).catch((error) => {
		res.status(500).send()
	})
})

app.delete('/files/:id', (req, res) => {
	const id = req.params.id;

	// Good practise: Validate the id
	if (!ObjectID.isValid(id)) {
		res.status(404).send();
	}

	File.findByIdAndRemove(id).then((file) => {
		if (!file) {
			res.status(404).send();
		} else {
			res.send(file);
		}
	}).catch((error) => {
		res.status(500).send();
	});
});




app.listen(port, () => {
	log(`Listening on port ${port}...`);
});