const mongoose = require('mongoose');

const mongoURI = process.env.MONGODB_URI || 'mongodb://localhost:27019/file-system';
mongoose.connect(mongoURI,{
        useNewUrlParser : true,
        useCreateIndex : true,
});

module.exports = {
        mongoose : mongoose,
};
