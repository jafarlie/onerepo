const mongoose = require('mongoose');


const mongoURI = process.env.MONGODB_URI || 'mongodb://localhost:27021/UserAPI';
// connect to our database
mongoose.connect(mongoURI, { useNewUrlParser: true});



module.exports = { mongoose };