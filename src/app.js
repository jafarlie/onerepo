'using strict'

const print = console.log;

/*Core Modules*/
const path = require('path');

/*NPM Modules*/
const express = require('express');
const app = express();

/*Directory Structure - Setting up middle ware*/
const rootPage = path.join(__dirname,'../pages');
app.use(express.static(rootPage));

const indexHtmlPage = path.join(rootPage, 'html/index.html');
app.get('', (req, res) => {
        res.status(200).sendFile(indexHtmlPage);
})

const adminHtmlPage = path.join(rootPage, 'html/admin.html');
app.get('/admin', (req, res) => {
        res.status(200).sendFile(adminHtmlPage);
})

const userHomeHtmlPage = path.join(rootPage, 'html/user-home.html');
app.get('/home', (req, res) => {
        res.status(202).sendFile(userHomeHtmlPage);
})

app.get('/json', (req, res) => {
        res.status(203).send([{name : 'JSON', version : '1.0'}, {name : 'Express', version : '2.0'}]);
})

app.listen(3001, () => {
        print('Server is up on port 3001');
});

