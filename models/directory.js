/* File Explorer DB Models */

const print = console.log;
const mongoose = require('mongoose');
const validator = require('validator');
const Schema = mongoose.Schema;
const {MongoClient, ObjectID} = require('mongodb');
const datetime = require('date-and-time')

/* Directory Model START */

/*
|_id|parent_ID|d_TIMESTAMP|d_NAME|d_ROOT|
*/

const Directory = mongoose.model('Directory', {
	parent_ID : { // Set by User. [HTML request]
		type : Schema.Types.ObjectId,
		validate : function (value) { //Custom Validation
                        if(!ObjectID.isValid(value)){
                                throw new Error(`Parent Directory ${value} is Invalid`);
                        }
			print(this);
                },
	},
	d_TIMESTAMP : { // Set by Mongoose Validator
		type : String,
	},
	d_NAME : { // Set by User. Need to set up a HTTP Endpoint for User to enter data
		type : String,
		required : true,
		trim : true,
		minlength : [5 , 'Directory name has to be at least greater than 5 characters'],
	},
	d_ROOT : { // Set by User.
		type : Boolean,
		default : false,
	},
	created_BY : {
                type : Schema.Types.ObjectId,
                validate : function (value) { //Custom Validation
                        if(!ObjectID.isValid(value)){
                                throw new Error(`File parent_ID ${value} is Invalid`);
                        }
                },
        } 

})

/* Directory Model END */

module.exports = {
	Directory : Directory,
}
