/* File Explorer DB Models */

const print = console.log;
const mongoose = require('mongoose');
const validator = require('validator');
const Schema = mongoose.Schema;
const {MongoClient, ObjectID} = require('mongodb');

/* File Model START */

/*
|_id|parent_ID|f_SIZE|f_TIMESTAMP|f_NAME|f_PATH|
*/

const File = mongoose.model('File', {
	parent_ID : { // Set by User. [HTML request]
		type : Schema.Types.ObjectId,
		validate : function (value) { //Custom Validation
                        if(!ObjectID.isValid(value)){
                                throw new Error(`File parent_ID ${value} is Invalid`);
                        }
                },
	},
	f_SIZE : { /*Always in bytes*/
		type : Number,
		default : 0,
	},
	f_TIMESTAMP : { // Set by Mongoose Validator
		type : Date,
		default : Date.now,
	},
	f_NAME : { // Set by User. Need to set up a HTTP Endpoint for User to enter data
		type : String,
		required : true,
		trim : true,
		minlength : [5 , 'Filename name has to be at least greater than 5 characters'],
	},
	f_PATH : { // Set by User.
		type : String,
		default : "/",
	},
	created_BY : {
		type : Schema.Types.ObjectId,
                validate : function (value) { //Custom Validation
                        if(!ObjectID.isValid(value)){
                                throw new Error(`File parent_ID ${value} is Invalid`);
                        }
                },
	} 

})

/* File Model END */

module.exports = {
	File,
}
