/* File Explorer DB Models */

const print = console.log;
const mongoose = require('mongoose');
const validator = require('validator');
const Schema = mongoose.Schema;
const {MongoClient, ObjectID} = require('mongodb');

/* User Drive Model START */

/*
|_id|u_ID|repo_ROOT|is_AUTH|auth_TOKEN|
*/

const UserDrive = mongoose.model('UserDrive', {
	u_ID : { // Set by User. [HTML request]
		type : Schema.Types.ObjectId,
		validate : function (value) { //Custom Validation
                        if(!ObjectID.isValid(value)){
                                throw new Error(`Parent Directory ${value} is Invalid`);
                        }
                },
	},
	repo_ROOT : {
		type : Schema.Types.ObjectId,
                validate : function (value) { //Custom Validation
                        if(!ObjectID.isValid(value)){
                                throw new Error(`Repo ROOT Directory ID ${value} is Invalid`);
                        }
                },
	},	
	is_AUTH : {
		type : Boolean,
		default : false,
	},
	auth_TOKEN {
		type : String,
		default : null,
	}
})

/* File Model END */

module.exports = {
	UserDrive
}
