/* user.js User model */

const mongoose = require('mongoose');


/* Schema
|_id|name|email|password|is_admin|
*/

const UserSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		minlength: 1,
		trim: true
	}, 
	email: {
		type: String,
		required: true,
		minlength: 1,
		trim: true,
		lowercase : true,
		validate : (email) => {
			if(!validator.isEmail(email)){
				throw new Error('Email is invalid');
			}
		}
	},
	password: {
		type: String,
		required: true,
		minlength: [8 , 'Password has to be atleast 8 characters'],
		trim: true
	},
	isAdmin: {
		type: Number,
		default: false,
	}
});

UserSchema.statics.findByEmailPassword = function(email, password) {
	const User = this;

	return User.findOne({email: email}).then((user) => {
		if (!user) {
			return Promise.reject();
		}

		return new Promise((resolve, reject) => {
			bcrypt.compare(password, user.password, (error, result) => {
				if (result) {
					resolve(user);
				} else {
					reject();
				}
			});
		});
	});
};


// This function runs before saving user to database
UserSchema.pre('save', function(next) {
	const user = this;

	if (user.isModified('password')) {
		bcrypt.genSalt(10, (error, salt) => {
			bcrypt.hash(user.password, salt, (error, hash) => {
				user.password = hash;
				next();
			});
		});
	} else {
		next();
	}

});

const User = mongoose.model('User', UserSchema);

module.exports = { 
	User 
};
