const print = console.log;
let globalLastId = 0;
let globalTableIndexTracker = 1;
const users = {
    "Elvin Jafarli":{
        userID: 1,
        email: "jafarli.elvin1997@gmail.com",
        phone: "6475726768",
        modified: "22.02.2019",
        password: "Iwtscf2d"
    },"Stas Pochnyok":{
        userID: 2,
        email: "stas1997@gmail.com",
        phone: "6475796768",
        modified: "22.02.2019",
        password: "Iwtscf2d"
    },"Nikita Jeffman":{
        userID: 3,
        email: "nikita1997@gmail.com",
        phone: "6475736576",
        modified: "22.02.2019",
        password: "Iwtscf2d"
    },"Mark Zuckerberg":{
        userID: 4,
        email: "mark.zuckerberg@fb.com",
        phone: "6475726761",
        modified: "22.02.2019",
        password: "Iwtscf2d"
    },"James Bond":{
        userID: 5,
        email: "james.bond1974@gmail.com",
        phone: "6475716868",
        modified: "22.02.2019",
        password: "Iwtscf2d"
    },"Jeniffer Lopes":{
        userID: 6,
        email: "jeniffer.lopes@outlook.com",
        phone: "6475746168",
        modified: "22.02.2019",
        password: "Iwtscf2d"
    },"Carlos Slim":{
        userID: 7,
        email: "carlos.coco@gmail.com",
        phone: "6475796268",
        modified: "22.02.2019",
        password: "Iwtscf2d"
    }
};

window.onload = function() {
      //populateTable();
      getUsersByAjax();
};
const addUserButton = document.getElementById("registerUserButton");
const deleteUserButton = document.getElementById("deleteUserButton");
const editUserButton = document.getElementById("editUserButton");
const adminDashboardRedirectButton = document.getElementById("admin-dashboard-nav-item");
adminDashboardRedirectButton.addEventListener('click', function(){location.href="../pages/admin.html";});
addUserButton.addEventListener('click', createUser);
deleteUserButton.addEventListener('click', deleteUser);
editUserButton.addEventListener('click', editUser);


const signOutButton = document.getElementById("sign-out-button");
signOutButton.addEventListener("click", function(){
	console.log("home clicked from profile page!!!!");
	document.location = "../pages/index.html";
});

function getUsersByAjax(){
    const url = '/usersForAdmin';
    fetch(url).then((res) => {
        if(res.status === 200){
            return res.json();
        }else{
            alert("Could not get users");
        }
    }).then((json) => {
        const userTable = document.getElementById("userDataTable");
        //print(json);
        json.users.map((user) => {
            const userName = user.name;
            const userEmail = user.email;
            const userID = user._id;
            const modifiedDate = user.modifiedDate.split("T")[0];
            
            const row = userTable.insertRow(globalTableIndexTracker);
            const idCell = row.insertCell(0);
            const nameCell = row.insertCell(1);
            const emailCell = row.insertCell(2);
            const modifiedCell = row.insertCell(3);
            const actionsBtn = row.insertCell(4);

            const userIDTextNode = document.createTextNode(userID);
            idCell.appendChild(userIDTextNode);
            const userNameTextNode = document.createTextNode(userName);
            nameCell.appendChild(userNameTextNode);
            const userEmailTextNode = document.createTextNode(userEmail);
            emailCell.appendChild(userEmailTextNode);
            const userModifiedTextNode = document.createTextNode(modifiedDate);
            modifiedCell.append(userModifiedTextNode);
            // button
            const divButtonWrapper = document.createElement('div');
            divButtonWrapper.className = "dropdown";
            const dropdownBtn = document.createElement('button');
            dropdownBtn.classList.add('btn');
            dropdownBtn.classList.add('btn-secondary');
            dropdownBtn.classList.add('dropdown-toggle');
            dropdownBtn.setAttribute('type', 'button');
            dropdownBtn.setAttribute('id', 'dropdownMenuButton');
            dropdownBtn.setAttribute('data-toggle', 'dropdown');
            dropdownBtn.setAttribute('aria-haspopup', 'true');
            dropdownBtn.setAttribute('aria-expanded', 'false');
            dropdownBtn.innerHTML = 'Actions';
            divButtonWrapper.appendChild(dropdownBtn);
            const dropdownButtonWrapper = document.createElement('div');
            dropdownButtonWrapper.className = 'dropdown-menu';
            dropdownButtonWrapper.setAttribute('aria-labelledby', 'dropdownMenuButton');
            const deleteBtn = document.createElement('button');
            deleteBtn.setAttribute('type', 'button');
            deleteBtn.setAttribute('id', 'admin-delete-user-nav-item-' + globalTableIndexTracker);
            deleteBtn.classList.add('btn');
            deleteBtn.classList.add('btn-danger');
            deleteBtn.setAttribute('data-toggle','modal');
            deleteBtn.setAttribute('data-target','#deleteModal');
            deleteBtn.innerHTML = "Delete";
    
            deleteBtn.style.cssFloat = 'left';
            deleteBtn.style.marginLeft = '1px';
            deleteBtn.style.width = '45%';

            dropdownButtonWrapper.appendChild(deleteBtn);
            const editBtn = document.createElement('button');
            editBtn.setAttribute('type', 'button');
            editBtn.setAttribute('id', 'admin-edit-user-nav-item-' + globalTableIndexTracker);
            editBtn.classList.add('btn');
            editBtn.classList.add('btn-warning');
            editBtn.setAttribute('data-toggle', 'modal');
            editBtn.setAttribute('data-target','#editModal');
            editBtn.innerHTML = "Edit";
            editBtn.style.cssFloat = 'right';
            editBtn.style.marginRight = '3px';
            editBtn.style.width = '50%';
            dropdownButtonWrapper.appendChild(editBtn);
            divButtonWrapper.appendChild(dropdownButtonWrapper);
            actionsBtn.append(divButtonWrapper);

            globalTableIndexTracker +=1;
        });
    }).catch((error) => {
        print(error);
    });
}
/*
function populateTable(){
    const userTable = document.getElementById("userDataTable");
    for(var name in users){
        const userName = name;
        const userEmail = users[name].email;
        const userID = users[name].userID;
        const phone = users[name].phone;
        const modifiedDate = users[name].modified;
        const row = userTable.insertRow(globalTableIndexTracker);
        const idCell = row.insertCell(0);
        const nameCell = row.insertCell(1);
        const emailCell = row.insertCell(2);
        const phoneCell = row.insertCell(3);
        const modifiedCell = row.insertCell(4);
        const userIDTextNode = document.createTextNode(userID);
        idCell.appendChild(userIDTextNode);
        const userNameTextNode = document.createTextNode(userName);
        nameCell.appendChild(userNameTextNode);
        const userEmailTextNode = document.createTextNode(userEmail);
        emailCell.appendChild(userEmailTextNode);
        const userPhoneTextNode = document.createTextNode(phone);
        phoneCell.appendChild(userPhoneTextNode);
        const userModifiedTextNode = document.createTextNode(modifiedDate);
        modifiedCell.append(userModifiedTextNode);
        globalTableIndexTracker +=1;
        globalLastId += 1;
    }
} */
function createUser(e){
    //getUsersByAjax();
    /*
    e.preventDefault();
    globalLastId += 1;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var yyyy = today.getFullYear();
    today = dd + "." + mm + "." + yyyy;
    const userName = document.getElementById("clientName").value;
    const userEmail = document.getElementById("clientEmail").value;
    const userPhone = document.getElementById("clientPhone").value;
    const userPassword = document.getElementById("clientPassword").value;
    users[userName] = {
        userID: globalLastId,
        email: userEmail,
        phone: userPhone,
        modified: today,
        password: userPassword
    };

    //We will need to make server here
    addUserToTable(userName, userEmail, userPhone, today);

    // close the modal
    $('#exampleModal').modal('hide'); */
}

function addUserToTable(userName, userEmail, userPhone, date){
    const userTable = document.getElementById("userDataTable");
    const row = userTable.insertRow(globalTableIndexTracker);
    const idCell = row.insertCell(0);
    const nameCell = row.insertCell(1);
    const emailCell = row.insertCell(2);
    const phoneCell = row.insertCell(3);
    const modifiedCell = row.insertCell(4);
    // Text Node of user data
    const userIDTextNode = document.createTextNode(globalLastId);
    const userNameTextNode = document.createTextNode(userName);
    const userEmailTextNode = document.createTextNode(userEmail);
    const userPhoneTextNode = document.createTextNode(userPhone);
    const userModifiedTextNode = document.createTextNode(date);
    // adding text nodes to the table
    idCell.appendChild(userIDTextNode);
    nameCell.appendChild(userNameTextNode);
    emailCell.appendChild(userEmailTextNode);
    phoneCell.appendChild(userPhoneTextNode);
    modifiedCell.appendChild(userModifiedTextNode);
}
/*
function deleteUser(e){
    //We will need to make server call for this function
    e.preventDefault();
    const chosenUserName = document.getElementById("userName").value;
    if(chosenUserName in users){
        removeFromTable(chosenUserName);
        delete users.chosenUserName;
        $('#deleteModal').modal('hide');
    }else{
        alert("Please type exact name");
    }  
} */

function removeFromTable(userName){
    //We will need to make server call for this function
    const userTable = document.getElementById("userDataTable");
    userTable.deleteRow(users[userName].userID);
}
/*
function editUser(e){
    //We will need to make server call for this function
    e.preventDefault();
    const chosenUserId = document.getElementById("editChosenClientID").value;
    const newSuggestedName = document.getElementById("editClientName").value;
    const newSuggestedEmail = document.getElementById("editClientEmail").value;
    const newSuggestedPhone = document.getElementById("editClientPhone").value;
    const newSuggestedPassword = document.getElementById("editClientPassword").value;
    if(newSuggestedName == "" && newSuggestedEmail == "" && newSuggestedPhone == "" && newSuggestedPassword == ""){
        $('#editModal').modal('hide');
        return;
    }
    if(chosenUserId == "" && (newSuggestedName != "" || newSuggestedEmail != "" || newSuggestedPhone != "" || newSuggestedPassword != "")){
        alert("Please select user id");
    }
    let suggestedCheckerName;
    let suggestedCheckerEmail;
    let suggestedCheckerPhone;
    let suggestedCheckerPassword;
    for(let key in users){
        if(users[key].userID == chosenUserId){
            if(newSuggestedEmail != ""){
                users[key].email = newSuggestedEmail;
                suggestedCheckerEmail = newSuggestedEmail;
            } else{
                suggestedCheckerEmail = users[key].email;
            }
            if(newSuggestedPhone != ""){
                users[key].phone = newSuggestedPhone;
                suggestedCheckerPhone = newSuggestedPhone;
            }else{
                suggestedCheckerPhone = users[key].phone;
            }
            if(newSuggestedPassword != ""){
                users[key].password = newSuggestedPassword;
                suggestedCheckerPassword = newSuggestedPassword;
            }else{
                suggestedCheckerPassword = users[key].password;
            }
            if(newSuggestedName != ""){
                suggestedCheckerName = newSuggestedName;
                let todayDate = new Date();
				let dd = todayDate.getDate();
				let mm = todayDate.getMonth() + 1; //January is 0!
                let yyyy = todayDate.getFullYear();
				if (dd < 10) {
				  dd = '0' + dd;
				} 
				if (mm < 10) {
				  mm = '0' + mm;
				} 
                todayDate = dd + '.' + mm + '.' + yyyy;
                if(newSuggestedEmail != ""){
                    suggestedCheckerEmail = newSuggestedEmail;
                }else{
                    suggestedCheckerEmail = users[key].email;
                }
                if(newSuggestedPhone != ""){
                    suggestedCheckerPhone = newSuggestedPhone;
                }else{
                    suggestedCheckerPhone = users[key].phone;
                }
                if(newSuggestedPassword != ""){
                    suggestedCheckerPassword = newSuggestedPassword;
                }else{
                    suggestedCheckerPassword = users[key].password;
                }
                users[newSuggestedName] = {
                    userID: users[key].userID,
                    email: suggestedCheckerEmail,
                    phone: suggestedCheckerPhone,
                    modified: todayDate,
                    password: suggestedCheckerPassword
                };
                print(users[newSuggestedName]);
            }else{
                suggestedCheckerName = key;
            }
        }
    }
	const userTable = document.getElementById("userDataTable");
	for (let i = 0; i < userTable.rows.length; i++){
		
		if (userTable.rows[i].cells[0].innerHTML == chosenUserId){
			userTable.rows[i].cells[1].innerHTML = suggestedCheckerName;
			userTable.rows[i].cells[2].innerHTML = suggestedCheckerEmail;
			userTable.rows[i].cells[3].innerHTML = suggestedCheckerPhone;
			var todayDate = new Date();
			var dd = todayDate.getDate();
			var mm = todayDate.getMonth() + 1; //January is 0!
			var yyyy = todayDate.getFullYear();
			if (dd < 10) {
			  dd = '0' + dd;
			} 
			if (mm < 10) {
			  mm = '0' + mm;
			} 
			todayDate = dd + '.' + mm + '.' + yyyy;
			userTable.rows[i].cells[4].innerHTML = todayDate;
		}	
	}
	$('#editModal').modal('hide');
}
*/
