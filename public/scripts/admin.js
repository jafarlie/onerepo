function getUserCountByAjax(){
    const url = '/adminUserCount';
    fetch(url).then((res) => {
      if(res.status === 200){
        return res.json();
      }else{
          alert("Could not get user count");
      }
    }).then((json) => {
      //console.log(json);
      const h2Id = document.getElementById("userCountH2");
      h2Id.innerHTML = json.length;
    });
  }
  
  function getLast3Users(){
    const url = '/adminLatest3Users';
    fetch(url).then((res) => {
      if(res.status === 200){
        return res.json();
      }else{
        alert("Could not fetch latest 3 users");
      }
    }).then((json) => {
      const userListLength = json.length;
      const userOne = json[0];
      const userTwo = json[1];
      const userThree = json[2];
      //dom users
      const domUserOne = document.getElementById("firstUserName");
      const domUserTwo = document.getElementById("secondUserName");
      const domUserThree = document.getElementById("thirdUserName");
      // dom email
      const domUserOneEmail = document.getElementById("firstUserEmail");
      const domUserTwoEmail = document.getElementById("secondUserEmail");
      const domUserThreeEmail = document.getElementById("thirdUserEmail");
      // dom date
      const domUserOneDateModified = document.getElementById("firstUserModifiedDate");
      const domUserTwoDateModified = document.getElementById("secondUserModifiedDate");
      const domUserThreeDateModified = document.getElementById("thirdUserModifiedDate");

      domUserOne.innerHTML = userOne.name;
      domUserTwo.innerHTML = userTwo.name;
      domUserThree.innerHTML = userThree.name;

      domUserOneEmail.innerHTML = userOne.email;
      domUserTwoEmail.innerHTML = userTwo.email;
      domUserThreeEmail.innerHTML = userThree.email;

      domUserOneDateModified.innerHTML = userOne.modifiedDate.split("T")[0];
      domUserTwoDateModified.innerHTML = userTwo.modifiedDate.split("T")[0];
      domUserThreeDateModified.innerHTML = userThree.modifiedDate.split("T")[0];

    });
  }

  getUserCountByAjax();
  getLast3Users();