var row

var drive
const initialize = new Request('/currentUserInfo', {
        method: 'get', 
        headers: {
            'Content-Type': 'application/json'
        },
    });
    fetch(initialize)
    .then((res) => { 
        if (res.status === 200) {
           return res.json() 
       } else {
            alert('Could not get user')
       }                
    })
    .then((json) => {
        
             document.getElementById("nameText").innerHTML = json.name
            document.getElementById("emailText").innerHTML = json.email
            if(json.googleDriveID == null){
              document.getElementById("gDriveButton").style.display = "none"
              document.getElementById("gDriveSpan").style.display = "inline"
              //console.log('in google drive')

            }
            if(json.oneDriveID==null){
              //console.log('in one drive')
              document.getElementById("1DriveButton").style.display = "none"
              document.getElementById("1DriveSpan").style.display = "inline"
            }


    })





function changeNameDom(e){


const url = '/currentUserInfo';

if (event.target.id == "gDriveButton"){

  drive = "google"



  row = event.target.parentNode.parentNode


}
if (event.target.id == "1DriveButton"){

  drive = "one"

  row = event.target.parentNode.parentNode


}




if (event.target.id == "saveName"){

  const request = new Request(url, {
        method: 'get', 
        headers: {
            'Content-Type': 'application/json'
        },
    });
    fetch(request)
    .then((res) => { 
        if (res.status === 200) {
           return res.json() 
       } else {
            alert('Could not get user')
       }                
    })
    .then((json) => {
        

        const id = json._id
        return json

    }).then((json) => {

      let data = {
        name: document.getElementById("nameInput").value,
        email: json.email,
        password: json.password,
        isAdmin: json.isAdmin,
        modifiedDate: new Date(),
        googleDriveID: json.googleDriveID,
        oneDriveID: json.oneDriveID
      }

      const patchReq = new Request('/user/profile/' + json._id, {

        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        }
      })
      fetch(patchReq).then((res) => {

        if(!res){

          console.log(error)

        }
        else{

          console.log(res)
        }

      }).catch((error) => {
        console.log(error)
    })
    
    })

       const str = document.getElementById("nameInput").value
       document.getElementById("nameText").innerHTML = str


  }

else if (event.target.id == "saveEmail"){
  const request = new Request(url, {
        method: 'get', 
        headers: {
            'Content-Type': 'application/json'
        },
    });
    fetch(request)
    .then((res) => { 
        if (res.status === 200) {
           return res.json() 
       } else {
            alert('Could not get user')
       }                
    })
    .then((json) => {
        

        const id = json._id
        return json

    }).then((json) => {

      let data = {
        name: json.name,
        email: document.getElementById("emailInput").value,
        password: json.password,
        isAdmin: json.isAdmin,
        modifiedDate: new Date(),
        googleDriveID: json.googleDriveID,
        oneDriveID: json.oneDriveID
      }

      const patchReq = new Request('/user/profile/' + json._id, {

        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        }
      })
      fetch(patchReq).then((res) => {

        if(!res){

          console.log(error)

        }
        else{

          console.log(res)
        }

      }).catch((error) => {
        console.log(error)
    })
    
    })


  const str = document.getElementById("emailInput").value
  document.getElementById("emailText").innerHTML = str
}


else if (event.target.id == "savePassword"){

  const old = document.getElementById('oldPasswordInput').value
  const new1 = document.getElementById('newPasswordInput').value
  const new2 = document.getElementById('confirmPasswordInput').value
  const errorPass = document.getElementById('passwordError')

  if (new1 != new2){
    errorPass.innerHTML = "Your new passwords do not match!"
    errorPass.style.display = "block"
  }

  else{


  const request = new Request(url, {
        method: 'get', 
        headers: {
            'Content-Type': 'application/json'
        },
    });
    fetch(request)
    .then((res) => { 
        if (res.status === 200) {
           return res.json() 
       } else {
            alert('Could not get user')
       }                
    })
    .then((json) => {
        

        const id = json._id
        return json

    }).then((json) => {

      let data = {
        name: json.name,
        email: json.email,
        password: new1,
        isAdmin: json.isAdmin,
        modifiedDate: new Date(),
        googleDriveID: json.googleDriveID,
        oneDriveID: json.oneDriveID
      }


      const patchReq = new Request('/user/profile/' + json._id, {

        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        }
      })
      

      const verifRequest = new Request('/verifyPassword/' + json._id, {

        method: 'post',
        body: JSON.stringify({"password": old}),

        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        }

      })


      fetch(verifRequest).then((bool) => {

        if (bool){
          console.log('old password is correct, changing password now')

          fetch(patchReq).then((res) => {

        if(!res){

          console.log(error)

        }
        else{
          errorPass.style.display = "block"
          errorPass.style.color= "green"
          errorPass.innerHTML = "Password Changed!!"
          console.log(res)
        }

      }).catch((error) => {
        console.log(error)
    
        })
    }

      


        else{


          errorPass.innerHTML = "Your old password is wrong!"
          errorPass.style.display = "block"

      console.log('you entered the wrong password')



        }

      })


      
    
    })
  

}
}

else if(event.target.id == "remAcc"){
  const request = new Request(url, {
        method: 'get', 
        headers: {
            'Content-Type': 'application/json'
        },
    });
    fetch(request)
    .then((res) => { 
        if (res.status === 200) {
           return res.json() 
       } else {
            alert('Could not get user')
       }                
    })
    .then((json) => {
      let data = {
        name: json.name,
        email: json.email,
        password: json.password,
        isAdmin: json.isAdmin,
        modifiedDate: new Date(),
        googleDriveID: json.googleDriveID,
        oneDriveID: json.oneDriveID
      }


      console.log(data)
      

      if(drive == 'google'){

        const url = '/directory/' + json.googleDriveID
        console.log('this is the url for the delete request' + url)
      const deleteDir = new Request(url, {

          method: 'delete',
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        }
      })

      fetch(deleteDir).then((res)=>{

        console.log('disconnected whatever drive you gave me')
      }).catch((error) =>{


        console.log('idk man some error occurred')
      }).catch((error)=>{

        console.log(error)
      })
      }else{
        const url = '/directory/' + json.oneDriveID
        console.log('this is the url for the delete request' + url)
      const deleteDir = new Request(url, {

          method: 'delete',
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        }
      })

      fetch(deleteDir).then((res)=>{

        console.log('disconnected whatever drive you gave me')
      }).catch((error) =>{


        console.log('idk man some error occurred')
      }).catch((error)=>{

        console.log(error)
      })


      }
      
      
     
      console.log('this is the user id before patching', json._id)
      console.log('this is one drive before patching', data.oneDriveID)
      const patchReq = new Request('/userDisconnectDrive/' + json._id, {

        method: 'post',
        body: JSON.stringify({"drive": drive}),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        }
      })
      fetch(patchReq).then((res) => {

        if(!res){

          console.log(error)

        }
        else{

          console.log(res)
        }

      }).catch((error) => {
        console.log(error)
    })

      
    
    })


 if(drive == 'google'){
              document.getElementById("gDriveButton").style.display = "none"
              document.getElementById("gDriveSpan").style.display = "inline"
              console.log('Disconnecting google drive')

            }
            if(drive == 'one'){
              console.log('Disconnecting one drive')
              document.getElementById("1DriveButton").style.display = "none"
              document.getElementById("1DriveSpan").style.display = "inline"
            }
}

}


document.addEventListener('click', changeNameDom)
