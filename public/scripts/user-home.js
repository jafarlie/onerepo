const print = console.log;

/* CLIENT SIDE FUNCTION */
/* Description : Callback function to call when HTML finishes loading */
window.onload = function() {
	
	init();
	
};

/* CLIENT SIDE FUNCTION */
/* Description : Initializes Sidebar. Initializes Overlays. Sets home-overlay as viewable. Hides other overlays */
function init(){

	/*Get Navbar scructure*/
	const oneRepo_nav_items = document.querySelectorAll('div#one-repo-userhome-navbar > ul > li.nav-item');
	const oneRepo_overlay_div = document.querySelectorAll('div#outer-overlay > div.overlay');
	
	/*Home is the default view*/
	if(oneRepo_nav_items.length > 0){
		/*Selects Home as active*/
		oneRepo_nav_items[0].querySelector('a.nav-link').classList.add('active');			
	} 
	if(oneRepo_overlay_div.length > 0){
		oneRepo_overlay_div[0].style.transition = "opacity .5s";
				oneRepo_overlay_div[0].style.opacity = "1";
				oneRepo_overlay_div[0].style.visibility = "visible";
	}

	const nav_link_home = document.querySelector('#one-repo-userhome-navbar');
	nav_link_home.addEventListener('click', show_overlay_panel);
	
	
	
	const path = window.location.pathname
	const pageName = path.split("/").pop()
	if (pageName != "profile.html"){
		/*Initializes Overlays */	
		//initialize_home_panel(); 
		initialize_file_panel();
	
	}
}


/* CLIENT SIDE FUNCTION */
/* Description : Populates home-overlay with summary of repos  */
function initialize_home_panel(){
	print('Initializing Home Panel');
	
	const oneRepo_home_container = document.querySelector('#home-container');
	
	const oneRepo_drop_down_div = document.createElement('div');
	oneRepo_drop_down_div.classList.add('accordion');
	oneRepo_drop_down_div.id = 'summaryAccordion';
	
	
	for(let repo in file_system.directories){ /*Get part of the file_system from server*/
		const repoInfoList = document.createElement("ul");
		const storageSpaceLi = document.createElement("li");
		const storageSpaceLiTextNode = document.createTextNode("Capacity: "  + file_system.directories[repo].repo.capacity); /*Get from server*/
		storageSpaceLi.appendChild(storageSpaceLiTextNode);
		const dateModifiedLi = document.createElement("li");
		const dateModifiedTextNode = document.createTextNode("Last Modified: " + file_system.directories[repo].repo.add); /*Get from server*/
		dateModifiedLi.appendChild(dateModifiedTextNode);
		const isAuthenticatedLi = document.createElement('li');
		const isAuthenticatedValue = file_system.directories[repo].repo.isAuthenticated; /*Obtained from Server*/

		let isAuthenticatedTextNodeValue;
		let isAuthenticatedButton = document.createElement("button");
		if(isAuthenticatedValue){
			isAuthenticatedTextNodeValue = document.createTextNode("Authenticated: Yes");
			isAuthenticatedLi.appendChild(isAuthenticatedTextNodeValue);
		}else{
			isAuthenticatedTextNodeValue = document.createTextNode("Authenticated: No");
			isAuthenticatedLi.appendChild(isAuthenticatedTextNodeValue);
			isAuthenticatedButton.className = "btn btn-primary cssOfAuthenticateButton";
			isAuthenticatedButton.appendChild(document.createTextNode("Authenticate"));
		}
		repoInfoList.appendChild(storageSpaceLi);
		repoInfoList.appendChild(dateModifiedLi);
		repoInfoList.appendChild(isAuthenticatedLi);


		if(isAuthenticatedButton.childNodes.length != 0){
			repoInfoList.appendChild(isAuthenticatedButton);
		}
		
		
		const progress_bar_wrapper = document.createElement('div');
		progress_bar_wrapper.classList.add('progress');

		const progress_bar = document.createElement('div');
		progress_bar.classList.add('progress-bar');
		progress_bar.classList.add('progress-bar-striped');
		progress_bar.classList.add('progress-bar-animated');
		progress_bar.classList.add('bg-success');
		progress_bar.setAttribute("role", "progressbar");
		progress_bar.setAttribute("aria-valuenow", "75");
		progress_bar.setAttribute("aria-valuemin", "0");
		progress_bar.setAttribute("aria-valuemax", "100");
		progress_bar.style.width = "75%";
		progress_bar.appendChild(document.createTextNode('75%'));
		progress_bar_wrapper.appendChild(progress_bar);
		

		const card = create_card_div(repo ,repoInfoList, oneRepo_drop_down_div.id, progress_bar_wrapper);
		oneRepo_drop_down_div.appendChild(card);	
	}
	
	

	oneRepo_home_container.appendChild(oneRepo_drop_down_div);

}

/* CLIENT SIDE FUNCTION */
/* Description : Populates file-overlay */
function initialize_file_panel(){
	print('Initializing File Panel');

  const oneRepo_file_container = document.querySelector('#file-container');

	const table = document.createElement('table');
	table.classList.add('table');
	table.classList.add('table-hover');
	table.setAttribute("id", "file-system-table");
	
	const table_head = document.createElement('thead');
	const table_head_row = document.createElement('tr');
	const name_header = document.createElement('th');
	name_header.appendChild(document.createTextNode('Name'));
	const date_header = document.createElement('th');
	date_header.appendChild(document.createTextNode('Date Modified'));
	const size_header = document.createElement('th');
	size_header.appendChild(document.createTextNode('Size'));	

	table_head_row.appendChild(name_header);
	table_head_row.appendChild(date_header);
	table_head_row.appendChild(size_header);
	table_head.appendChild(table_head_row);
	table.appendChild(table_head);

	const table_body_0 = document.createElement('tbody');
        table_body_0.setAttribute("id", "back-button");
        table.appendChild(table_body_0);

	const table_body_1 = document.createElement('tbody');
	table_body_1.setAttribute("id", "directories");
	table.appendChild(table_body_1);

	const table_body_2 = document.createElement('tbody');
        table_body_2.setAttribute("id", "files");
        table.appendChild(table_body_2);

	const table_body_3 = document.createElement('tbody');
        table_body_3.setAttribute("id", "add-file-dir");
        table.appendChild(table_body_3);

	oneRepo_file_container.appendChild(table);
	document.getElementById("back-button").addEventListener("click", file_system_dec);
	document.getElementById("directories").addEventListener("click", file_system_dec);
	document.getElementById("files").addEventListener("click", file_system_dec);
	//document.getElementById("add-file-dir").removeEventListener("click", file_system_dec);
	refresh_all('000000000000000000000000', '000000000000000000000000');
}

function clearDirectories(){
	const table_body = document.querySelector('table#file-system-table > tbody#directories');
        const table_body_rows = table_body.querySelectorAll('tr');
        if(table_body_rows.length != 0){
                for(let i = 0; i < table_body_rows.length; i++){
                        table_body.removeChild(table_body_rows[i]);
                }
        }
}

function clearFileAdds(){
	const table_body = document.querySelector('table#file-system-table > tbody#add-file-dir');
        const table_body_rows = table_body.querySelectorAll('tr');
        if(table_body_rows.length != 0){
                for(let i = 0; i < table_body_rows.length; i++){
                        table_body.removeChild(table_body_rows[i]);
                }
        }
}

function addFileDirButt(){
	
	const table_body = document.querySelector('table#file-system-table > tbody#add-file-dir');
        const table_body_rows = table_body.querySelectorAll('tr');
        if(table_body_rows.length != 0){
                for(let i = 0; i < table_body_rows.length; i++){
                        table_body.removeChild(table_body_rows[i]);
                }
        }

	const op_row = document.createElement('tr');
        const file_upload_cell = document.createElement('td');
	const dir_create_cell = document.createElement('td');
	const blank_cell = document.createElement('td');

	//UPLOAD BEGIN -----------------------------------------------------		
	//const file_body = document.querySelector('#file-container');
	const form = document.createElement('form');

	const currentDirectoryID = document.querySelector('table#file-system-table').getAttribute('current-node');
	
		
	//Form
	form.setAttribute("id", "file-add-form");
	form.setAttribute("enctype", "multipart/form-data");
	form.setAttribute("method", "post");
	const post_url = '/file/' + currentDirectoryID;
	form.setAttribute("action", post_url);
	
	
	const upload_wrapper = document.createElement("div");
	//upload_wrapper.classList.add('custom-file');
	//upload_wrapper.classList.add('mb-3');

	//Button
	const upload_btn = document.createElement("button");
	upload_btn.setAttribute("type", "submit");
	//upload_btn.classList.add('input-group-text');
	//upload_btn.classList.add('mb-3');
	upload_btn.setAttribute("id", "upload-button");
	upload_btn.appendChild(document.createTextNode("Upload"));
	
	/* HADI COMMENT THIS OUT TO ACTUALLY UPLOAD */
	//upload_btn.addEventListener("click", upload_file);
	
	

	//Input 
	const browse_input = document.createElement("input");
        browse_input.setAttribute("type", "file");
        //browse_input.classList.add('custom-file-input');
        browse_input.setAttribute("id", "file-add");
	browse_input.setAttribute("name", "file");

	//Label
	const browse_text_label = document.createElement("label");
	browse_text_label.classList.add('sr-only');
        browse_text_label.setAttribute("for","file-add");
	browse_text_label.appendChild(document.createTextNode("Choose File"));

	//div1.appendChild(upload_btn);
	upload_wrapper.appendChild(upload_btn);
	upload_wrapper.appendChild(browse_input);
	upload_wrapper.appendChild(browse_text_label);
	//div1.appendChild(upload_wrapper);
	form.appendChild(upload_wrapper);

	file_upload_cell.appendChild(form);
	op_row.appendChild(file_upload_cell);
	//UPLOAD END -----------------------------------------------------

	op_row.appendChild(blank_cell);

	//DIR BEGIN -----------------------------------------------------

	const dir_text_input =  document.createElement("input");
        dir_text_input.setAttribute("type", "text");
	dir_text_input.setAttribute("id", "directory-add");
	
	//Button
        const dir_make_btn = document.createElement("button");
        //upload_btn.classList.add('input-group-text');
        //upload_btn.classList.add('mb-3');
        dir_make_btn.setAttribute("id", "make-dir-button");
        dir_make_btn.appendChild(document.createTextNode("Create Directory"));


	dir_create_cell.appendChild(dir_text_input);
	dir_create_cell.appendChild(dir_make_btn);

	dir_make_btn.addEventListener("click", make_directory);

	op_row.appendChild(dir_create_cell);
	

	//DIV END -----------------------------------------------------

	
	table_body.appendChild(op_row);
}

function addFileDirButtNU(){

        const table_body = document.querySelector('table#file-system-table > tbody#add-file-dir');
        const table_body_rows = table_body.querySelectorAll('tr');
        if(table_body_rows.length != 0){
                for(let i = 0; i < table_body_rows.length; i++){
                        table_body.removeChild(table_body_rows[i]);
                }
        }

        const op_row = document.createElement('tr');
        const file_upload_cell = document.createElement('td');
        const dir_create_cell = document.createElement('td');
        const blank_cell = document.createElement('td');

        //UPLOAD BEGIN -----------------------------------------------------
        //const file_body = document.querySelector('#file-container');

        op_row.appendChild(file_upload_cell);
        //UPLOAD END -----------------------------------------------------

        op_row.appendChild(blank_cell);

        //DIR BEGIN -----------------------------------------------------

        const dir_text_input =  document.createElement("input");
        dir_text_input.setAttribute("type", "text");
        dir_text_input.setAttribute("id", "directory-add");

        //Button
        const dir_make_btn = document.createElement("button");
        //upload_btn.classList.add('input-group-text');
        //upload_btn.classList.add('mb-3');
        dir_make_btn.setAttribute("id", "make-dir-button");
        dir_make_btn.appendChild(document.createTextNode("Create Directory"));


        dir_create_cell.appendChild(dir_text_input);
        dir_create_cell.appendChild(dir_make_btn);

        dir_make_btn.addEventListener("click", make_directory);

        op_row.appendChild(dir_create_cell);
		//DIV END -----------------------------------------------------


        table_body.appendChild(op_row);
}

function addDirectories(dir_array){
const table_body = document.querySelector('table#file-system-table > tbody#directories');
const table_body_rows = table_body.querySelectorAll('tr');
if(table_body_rows.length != 0){
	for(let i = 0; i < table_body_rows.length; i++){
		table_body.removeChild(table_body_rows[i]);
	}
}

dir_array.forEach((object) => {
	//print(object);
	const dir_row = document.createElement('tr');
	const dir_name_cell = document.createElement('td');
	const icon_wrapper = document.createElement("div")
	const icon = document.createElement("img");
	icon.src = "../images/folder.png";
	icon.style.width = "20px";
	icon.style.height = "20px";
	icon_wrapper.style.display = "inline";
	icon_wrapper.style.margin = "0 10px 0 0";
	icon_wrapper.appendChild(icon);

	dir_name_cell.appendChild(icon_wrapper);
	dir_name_cell.appendChild(icon_wrapper);
	dir_name_cell.appendChild(document.createTextNode(object.d_NAME));

	const dir_mod_date_cell = document.createElement('td');
	if(object.hasOwnProperty('d_TIMESTAMP')){
		dir_mod_date_cell.appendChild(document.createTextNode(object.d_TIMESTAMP));
	}else{
		dir_mod_date_cell.appendChild(document.createTextNode('-'));
	}

	const dir_size_cell = document.createElement('td');
	if(object.hasOwnProperty('size')){
		dir_size_cell.appendChild(document.createTextNode(object.size));
	}else{
		dir_size_cell.appendChild(document.createTextNode('-'));
	} 
	if(object.hasOwnProperty('_id')){
		dir_row.setAttribute('target-node', object._id);
		dir_row.classList.add('directory');
	}
	dir_row.appendChild(dir_name_cell);
	dir_row.appendChild(dir_mod_date_cell);
	dir_row.appendChild(dir_size_cell);
	table_body.appendChild(dir_row);

});
}

function addBackButtonPrevId(current_node, target_node){

	const table_body = document.querySelector('table#file-system-table > tbody#back-button');
	const table_body_rows = table_body.querySelectorAll('tr');
	if(table_body_rows.length != 0){
		for(let i = 0; i < table_body_rows.length; i++){
			table_body.removeChild(table_body_rows[i]);
		}
	}

	if(target_node == '000000000000000000000000'){
		return;
	}

	const dir_row = document.createElement('tr');
	const dir_back_cell = document.createElement('td');
	const icon_wrapper = document.createElement("div")
	const icon = document.createElement("img");
	icon.src = "../images/back.png";
	icon.style.width = "20px";
	icon.style.height = "20px";
	icon_wrapper.style.display = "inline";
	icon_wrapper.style.margin = "0 10px 0 0";
	icon_wrapper.appendChild(icon);

	dir_back_cell.appendChild(icon_wrapper);
	dir_back_cell.setAttribute("colspan", "3");
	dir_row.appendChild(dir_back_cell);
	dir_row.classList.add('back');

	dir_row.setAttribute('target-node', current_node);

	table_body.appendChild(dir_row);	


}

function clearBackButton(){
const table_body = document.querySelector('table#file-system-table > tbody#back-button');
const table_body_rows = table_body.querySelectorAll('tr');
if(table_body_rows.length != 0){
	for(let i = 0; i < table_body_rows.length; i++){
		table_body.removeChild(table_body_rows[i]);
	}
}
}

function clearFiles(){
const table_body = document.querySelector('table#file-system-table > tbody#files');
const table_body_rows = table_body.querySelectorAll('tr');
if(table_body_rows.length != 0){
	for(let i = 0; i < table_body_rows.length; i++){
		table_body.removeChild(table_body_rows[i]);
	}
}
}

function addFiles(file_array){

const table_body = document.querySelector('table#file-system-table > tbody#files');
const table_body_rows = table_body.querySelectorAll('tr');
if(table_body_rows.length != 0){
	for(let i = 0; i < table_body_rows.length; i++){
		table_body.removeChild(table_body_rows[i]);
	}
}

file_array.forEach((object) => {
	
	const file_row = document.createElement('tr');
	const file_name_cell = document.createElement('td');
	file_name_cell.classList.add('file-name');
	const icon_wrapper = document.createElement("div")
	const icon = document.createElement("img");
	icon.src = "../images/file.png";
	icon.style.width = "20px";
	icon.style.height = "20px";
	icon_wrapper.style.display = "inline";
	icon_wrapper.style.margin = "0 10px 0 0";
	icon_wrapper.appendChild(icon);

	file_name_cell.appendChild(icon_wrapper);
	file_name_cell.appendChild(document.createTextNode(object.filename));
	
	const file_mod_date_cell = document.createElement('td');
	if(object.hasOwnProperty('date_modified')){
			file_mod_date_cell.appendChild(document.createTextNode(object.date_modified));
	}else{
			file_mod_date_cell.appendChild(document.createTextNode('-'));
	}

	const file_size_cell = document.createElement('td');
	if(object.hasOwnProperty('size')){
			file_size_cell.appendChild(document.createTextNode(object.size));
	}else{
			file_size_cell.appendChild(document.createTextNode('-'));
	}
		
	const drop_down_div = document.createElement('div');
	drop_down_div.classList.add('btn-group');
	drop_down_div.classList.add('dropleft');
	drop_down_div.classList.add('fs-drop-down');
	
	const drop_button = document.createElement('button');
	drop_button.type = 'button';
	drop_button.classList.add('btn');
	//drop_button.classList.add('btn-primary');
	//drop_button.classList.add('dropdown-toggle');
	drop_button.setAttribute('data-toggle', 'dropdown');
	drop_button.setAttribute('aria-haspopup', 'true');
	drop_button.setAttribute('aria-expanded','false');
	const burger_menu = document.createElement('i');
	burger_menu.classList.add('fa');
	burger_menu.classList.add('fa-bars');
	drop_button.appendChild(burger_menu);
	
	const drop_down_menu = document.createElement('div');
	drop_down_menu.classList.add('dropdown-menu');
	const drop_down_file_del = document.createElement('a');
	drop_down_file_del.classList.add('dropdown-item');
	drop_down_file_del.href = '#';
	drop_down_file_del.classList.add('delete-file');
	drop_down_file_del.appendChild(document.createTextNode('Delete'))

	const drop_down_file_download = document.createElement('a');
	drop_down_file_download.classList.add('dropdown-item');
        drop_down_file_download.href = '#';
        drop_down_file_download.classList.add('download-file');
        drop_down_file_download.appendChild(document.createTextNode('Download'));	

	//drop_down_file_del.style.color = 'red';
	drop_down_menu.appendChild(drop_down_file_download)
	drop_down_menu.appendChild(drop_down_file_del)


	drop_down_div.appendChild(drop_button)
	drop_down_div.appendChild(drop_down_menu)
	file_size_cell.appendChild(drop_down_div);

	file_row.appendChild(file_name_cell);
	file_row.appendChild(file_mod_date_cell);
	file_row.appendChild(file_size_cell);
	file_row.classList.add('file');

	if(object.hasOwnProperty('id')){ //Why not _id?
		file_row.setAttribute('target-node', object.id);
		file_row.classList.add('file');
	}

	table_body.appendChild(file_row);
})


}

function addFilesByPid(pID){
const url = '/filesByParentID/'+pID;

const directories = fetch(url).then((res) => {
	if(res.status === 200) {
		return res.json();
	}else{
		alert('Could not get directories');
	}
}).then((json) => {
	addFiles(json);
}).catch((error) => {
	return print(error);
})
}

function addDirectoryByPid(pID){

	const url = '/directoriesByParentID/'+pID;

	const directories = fetch(url).then((res) => {
		if(res.status === 200) {
			return res.json();
		}else{
			alert('Could not get directories');
		}
	}).then((json) => {
		addDirectories(json);
	}).catch((error) => {
		return print(error);
	})
}

function executeBackAction(target_node){
	
	if(target_node === '000000000000000000000000'){
		clearBackButton();
		clearFiles();
		clearDirectories();
		clearFileAdds();
		addDirectoryByPid(target_node);
		addFileDirButtNU();
		const table = document.querySelector('table#file-system-table');
		table.setAttribute('current-node', target_node);
		return;
	}

	const url = '/directory/'+target_node;
	//print(url);
	const request = new Request(url, {
		method : 'get',
		headers : {
			'Accept' : 'application/json, text/plain, */*',
			'Content-Type' : 'application/json'
		},
	});	

	fetch(request).then((res) => {
		if(res.status === 200) {
			return res.json();
		}else{
			alert('Could not get directories');
		}
	}).then((json) => {
		const previousDirID = json._id;
		const previousDirPID = json.parent_ID;
	
		refresh_all(previousDirPID, target_node);
		const table = document.querySelector('table#file-system-table');
		table.setAttribute('current-node', previousDirID);		

	}).catch((error) => {
		return print(error);
	})
}

const profileButton = document.getElementById("profile-button-item");
if(profileButton != null){
profileButton.addEventListener("click", function(){
	console.log("profile clicked!!!!");
	document.location = "../pages/profile.html";
});
}

/*

3- POST http://localhost:3000/directory

body:
{
        "parent_ID" : "000000000000000000000000",
        "d_NAME" : "Photos",
        "d_ROOT" : false
}

*/
function make_directory(e){
	e.preventDefault();
	const input = document.querySelector('table#file-system-table > tbody#add-file-dir > tr > td > input#directory-add');
	const directory_name = input.value;
	const currentDirectoryID = document.querySelector('table#file-system-table').getAttribute('current-node');
	const url = '/directory/';

	const data = {
		parent_ID : currentDirectoryID,
		d_NAME : directory_name,
		d_ROOT : false
	};
	
	const request = new Request(url, {
		method : 'post',
		body : JSON.stringify(data),
		headers : {
			'Accept' : 'application/json, text/plain, */*',
			'Content-Type' : 'application/json'
		},
	});

	fetch(request).then((res)=> {
		addDirectoryByPid(currentDirectoryID);	
	}).catch((error)=> {
		alert(error);
	});
}

/* SERVER SIDE FUNCTION */
/* Description : To add/upload the file to a repo*/
function upload_file(e){
	e.preventDefault();
	print('Adding File');
	//const form = document.querySelector('form#file-add');	

	const formData = new FormData(document.getElementById('form#file-add'));
	print(formData);


	const currentDirectoryID = document.querySelector('table#file-system-table').getAttribute('current-node');
        const url = '/file/' + currentDirectoryID;


	const request = new Request(url, {
        method: 'post', 
        body: JSON.stringify(formData),
        	headers: {
            		'Accept': 'application/json, text/plain, */*',
            		'Content-Type': 'application/json'
        	},
    	});	
	/*
	const fileName = document.querySelector('input#file-add').value;
	print(fileName);
	if(fileName){
		document.getElementById('file-add').submit()
		const table_body = document.querySelector('table#file-system-table > tbody');
	if(table_body.hasAttribute('current-node')){
		const current_node = table_body.getAttribute('current-node');
		const directory_ref = return_directory(current_node, file_system); 
	if(directory_ref.hasOwnProperty('files')){
		directory_ref.files[fileName] = {};
	}else{
		directory_ref['files'] = {}; 
		directory_ref.files[fileName] = {};
	}
	}
	}else{
		console.log('Error : Empty');
	}*/
}

/* Half CLIENT and Half Server SIDE FUNCTION */
/* Description : Populate file system. All the get requests for the file system should be server side*/
function populate_table_body(directory_json){

const table_body = document.querySelector('table#file-system-table > tbody');
const table_body_rows = table_body.querySelectorAll('tr'); 
if(table_body_rows.length != 0){
	for(let i = 0; i < table_body_rows.length; i++){
		table_body.removeChild(table_body_rows[i]);
	}
}


if(directory_json.hasOwnProperty('node')){
	if(directory_json.node != 0){
		const dir_row = document.createElement('tr');
		const dir_back_cell = document.createElement('td');
		const icon_wrapper = document.createElement("div")
		const icon = document.createElement("img");
		icon.src = "../images/back.png";
		icon.style.width = "20px";
		icon.style.height = "20px";
		icon_wrapper.style.display = "inline";
		icon_wrapper.style.margin = "0 10px 0 0";
		icon_wrapper.appendChild(icon);

		dir_back_cell.appendChild(icon_wrapper);
		dir_back_cell.setAttribute("colspan", "3");
		dir_row.appendChild(dir_back_cell);
		dir_row.classList.add('back');
		table_body.appendChild(dir_row);
	}
		table_body.setAttribute('current-node', directory_json.node);
}

if(directory_json.hasOwnProperty('node')){
			if(directory_json.node != 0){
		
		const dir_row = document.createElement('tr');
					const dir_add_cell = document.createElement('td');
		dir_add_cell.classList.add('add-files')

		if (!document.getElementById('file-add')){
		const form = document.createElement('form');
		//form.classList.add('form-inline');
		form.id = 'file-add';
		form.enctype = 'multipart/form-data'
		
		const label = document.createElement('label');
		label.classList.add("sr-only");
		label.setAttribute('for', 'file-add');
		label.appendChild(document.createTextNode('Name'));

		const input = document.createElement('input')
		input.type='file';
		input.name='file'
		form.method='post';
		form.action='http://localhost:3000/files/123'	
		//input.classList.add('form-control')
		//input.classList.add('mb-2')
		//input.classList.add('mr-sm-2')
		input.id = 'file-add';
		

		const button = document.createElement('button');
		button.type = 'submit';
		button.classList.add('btn');
		button.classList.add('btn-primary');
		button.classList.add('mb-2');
		button.appendChild(document.createTextNode('Add File'))
		button.addEventListener('click', add_file)

		form.appendChild(label)
		form.appendChild(input)
		form.appendChild(button);

		//dir_add_cell.setAttribute("colspan", "3");
		//dir_add_cell.appendChild(form)
		//dir_row.appendChild(dir_add_cell)
		document.getElementById('file-container').appendChild(form)	
		}		

			}
	}

}

/* SERVER SIDE FUNCTION */
/* Description : Returns directory when providing a directory.node value */
function return_directory(node, directory){
if(directory.node == node){
	return directory;
}else if(directory.hasOwnProperty('directories')){
	let count = 0;
	let dirs = '';
	for(dirs in directory.directories){
		const ret_dir = return_directory(node,directory.directories[dirs]);
		if(ret_dir != -1){
			return ret_dir;
		}
		
		count++;	
	}
	return -1;
}else{
	return -1;
}
}

/* SERVER SIDE FUNCTION */
/* Description : Returns parent node in of directory*/
function find_parent_node(node_id, directory){
if(node_id == 0){
	return 0;
}else{
	if(directory.hasOwnProperty('directories')){
		let dirs = '';
		for(dirs in directory.directories){
			if(directory.directories[dirs].hasOwnProperty('node')){
				/* 
				print('Dir:'+ dirs);
				print('Node:' +directory.directories[dirs].node);
				print('Current Id: ' +node_id );
				print('LParent Id: ' + directory.node);
				*/
				if(directory.directories[dirs].node == node_id){
					return directory.node;
				}else{
					const ret_val = find_parent_node(node_id,directory.directories[dirs]);
					if(ret_val){
						return ret_val;
					}
				}						
			}
		}		
	}
}
}


function refresh_all(cN,tN){
	
	const table = document.querySelector('table#file-system-table');
	table.setAttribute('current-node', tN);
	addBackButtonPrevId(cN,tN);
    	addDirectoryByPid(tN); /*Get directories from server*/
        if(tN != '000000000000000000000000'){
		addFilesByPid(tN); /*Get files from server*/
        }
	if(tN == '000000000000000000000000'){
		addFileDirButtNU();
	}else{
		addFileDirButt();
	}
	setup_ajaxForm(cN,tN);
}

function setup_ajaxForm(cN,tN){
	$('#file-add-form').ajaxForm(function() {
		refresh_all(cN,tN);
	});
}

/* CLIENT SIDE FUNCTION */
/* Description : Makes decision based on events like clicking on the file explorer */
function file_system_dec(e){
	e.preventDefault();
	//const test = return_directory(2,file_system);

	if (!(e.target.parentElement.classList.contains('directory') 
				|| e.target.parentElement.parentElement.parentElement.classList.contains('back')
				|| e.target.classList.contains('delete-file')
				|| e.target.classList.contains('download-file'))){
		return;
	}

	if(e.target.parentElement.classList.contains('directory')){ // If you are clicking on a directory [WORKS]
		if(e.target.parentElement.hasAttribute('target-node')){
			const target_node = e.target.parentElement.getAttribute('target-node');
			const table = document.querySelector('table#file-system-table');
			const current_node = table.getAttribute('current-node');
			table.setAttribute('current-node', target_node);
			
			refresh_all(current_node,target_node);
			setup_ajaxForm(current_node,target_node);
		}
		return;
	}

	if(e.target.parentElement.parentElement.parentElement.classList.contains('back')){
		//const table_body = document.querySelector('table#file-system-table > tbody');
		//if(table_body.hasAttribute('current-node')){
			
			//const current_node = table_body.getAttribute('current-node');
			//const parent_node = find_parent_node(current_node, file_system); /*Get from server*/
			//print('Current Node: ' + current_node + '| Parent Node: ' + parent_node); 
			//populate_table_body(return_directory(parent_node,file_system)); /*Get parent dir from server*/
		//}
		const target_node = e.target.parentElement.parentElement.parentElement.getAttribute('target-node');
		executeBackAction(target_node);	
	}

	/*There is a weird bug here. Sometimes you cannot delete a file*/
	if(e.target.classList.contains('delete-file')){
		const parent_row = e.target.parentElement.parentElement.parentElement.parentElement;
		const target_node = parent_row.getAttribute('target-node');
		const file_name = parent_row.querySelectorAll('td.file-name')[0].innerText;
		const table = document.querySelector('table#file-system-table');
		const current_node = table.getAttribute('current-node');
		//DELETE '/file/:id'
		const url = '/file/' + target_node;
		
		const request = new Request(url, {
			method : 'delete'
		});

		print('Deleting')
		fetch(request).then((res) => {
			addFilesByPid(current_node);
			print('Deleted')
		}).catch((error) => {
			alert(error);
		})
		
		/*
		const table_body = document.querySelector('table#file-system-table > tbody');
		if(table_body.hasAttribute('current-node')){
			const current_node = table_body.getAttribute('current-node');
			const directory_ref = return_directory(current_node, file_system);
			if(directory_ref.hasOwnProperty('files')){
				delete directory_ref.files[file_name]; 
			}else{
				print('Can"t delete');
			}
			populate_table_body(directory_ref);
		}
		*/
	}

	if(e.target.classList.contains('download-file')){
		print("Downloading");
		const parent_row = e.target.parentElement.parentElement.parentElement.parentElement;
		const name = parent_row.querySelectorAll('td')[0].innerText;
		const target_node = parent_row.getAttribute('target-node')	
		const url = '/fileDownload/' + target_node;
		
		print(url);
		print(name);
		downloadURI(url,name);	
		/*fetch(url, {method : 'GET'}).then((res) => {
			if(res.status === 200){
				print('Downloaded');
			}else{
				alert('Could not download');
			}
		}).catch((error) => {
			print(error);
		});*/
	}
}

/*From Stack Exchange*/
function downloadURI(url, name) {
	const link = document.createElement('a');
	link.download = name;
	link.href = url;
	link.click();
	delete link;
}


/* CLIENT SIDE FUNCTION */
/* Description : Creates collapsible cards from repo summary page.*/
/*https://getbootstrap.com/docs/4.1/components/collapse/*/
function create_card_div(header, body, data_parent, header_elements){
	const card = document.createElement('div');
	card.classList.add('card');

	const adjusted_header = header.replace(/\s+/g, '-');

	/*Set up Card Header BEGIN*/
	const card_header = document.createElement('div');
  card_header.classList.add('card-header');
	card_header.id='card-heading-'+adjusted_header;

	const header_h5 = document.createElement('h5');
  header_h5.classList.add('mb-0');

	const header_button = document.createElement('button');
  header_button.classList.add('btn');	
	header_button.classList.add('btn-link');
	header_button.id = header+"-header-button";
	header_button.type = "button";
	header_button.setAttribute("data-toggle", "collapse");
	header_button.setAttribute("data-target", "#collapse-"+adjusted_header);
	header_button.setAttribute("aria-expanded", "false");
	header_button.setAttribute("aria-controls", "collapse-"+adjusted_header);

	header_button.appendChild(document.createTextNode(header));
	
	header_h5.appendChild(header_button);
	header_h5.appendChild(header_elements);
	
	card_header.appendChild(header_h5);

	/*Set up Card Header END*/

	/*Set up Card BODY BEGIN*/
	
	const card_body_div = document.createElement('div');
	card_body_div.classList.add('card-body');
	card_body_div.appendChild(body);

	const card_body = document.createElement('div');
	card_body.classList.add('collapse');
	card_body.appendChild(card_body_div);
	card_body.id = "collapse-"+adjusted_header;
	card_body.setAttribute("aria-labelledby",'card-heading-'+adjusted_header);
	card_body.setAttribute("data-parent", "#"+data_parent);
	/*Set up Card BODY END*/	
	card.appendChild(card_header);
	card.appendChild(card_body);

	return card;
}
/* CLIENT SIDE FUNCTION */
/* Description : Switches between different panels without redirection to another page. */
function show_overlay_panel(e){
	e.preventDefault();
	if (!e.target.classList.contains('nav-link')){
		return;
	}
	if (e.target.id == "home-button"){
		document.location = "../pages/user-home.html";
		return;
	}
	const oneRepo_nav_li = e.target.parentElement.parentElement.querySelectorAll('li > a');

	const oneRepo_overlay_div = document.querySelectorAll('div#outer-overlay > div.overlay');		

	for(let i = 0; i < oneRepo_nav_li.length; i++){
		if(oneRepo_nav_li[i] === e.target){
			oneRepo_overlay_div[i].style.transition = "opacity .5s";
	  oneRepo_overlay_div[i].style.opacity = "1";
	  oneRepo_overlay_div[i].style.visibility = "visible";
			e.target.classList.add('active');
			hide_other_overlays_except(i);
			hide_other_active_navs_except(i);
			break;
		}
	}
}

/* CLIENT SIDE FUNCTION */
/* Description : Prevents multiple overlays from being displayed*/
function hide_other_overlays_except(div_number){
	const oneRepo_overlay_div = document.querySelectorAll('div#outer-overlay > div.overlay');
	
	for(let i = 0; i < oneRepo_overlay_div.length; i++){
		if(i != div_number){
			oneRepo_overlay_div[i].style.transition = "opacity .5s";
			oneRepo_overlay_div[i].style.opacity = "0";
					oneRepo_overlay_div[i].style.visibility = "hidden";
			}
  }
}

/* CLIENT SIDE FUNCTION */
/* Description : Makes the nav links active to highlight what overlays you are currently viewing */
function hide_other_active_navs_except(nav_number){
	const oneRepo_nav_div = document.querySelectorAll('div#one-repo-userhome-navbar > ul > li.nav-item > a.nav-link');
	
	for(let i = 0; i < oneRepo_nav_div.length; i++){
		if(i != nav_number){
			oneRepo_nav_div[i].classList.remove('active');
			}
  }

}

