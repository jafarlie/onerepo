// sign-in functionality
const users = {
    "user":{
        userID: 1,
        email: "user@gmail.com",
        phone: "6470000000",
        modified: "22.02.2019",
        password: "user"
    },"admin":{
        userID: 2,
        email: "admin@gmail.com",
        phone: "6471111111",
        modified: "22.02.2019",
        password: "admin"
    }
}

document.getElementById("signInBtn").addEventListener('click', verifyCredentials)
document.getElementById("closeBtnSignInModal").addEventListener('click', closeModal)
document.getElementById("closeX").addEventListener('click', closeModal)


function verifyCredentials(){
	//We will need to make server call to verify credentials
	console.log("here")
	const loginModal = document.getElementById("logInModalCenter")
	
	const username = loginModal.querySelector("#clientLoginEmail").value
	const pass = loginModal.querySelector("#clientLoginPassword").value
	if ( username in users && users[username].password == pass){
		
		if (username == "user"){
			
			document.location = "../pages/user-home.html";
			
		}
		else if (username == "admin"){
			
			
			document.location = "../pages/admin.html";
			
		}
	
	
	}
	else if (document.getElementById("wrongCredentials") == null){
		
		
		rememberMe = document.getElementById("rememberMe")
		const span = document.createElement("span")
		span.id = "wrongCredentials"
		span.style.color = "red"
		span.style.display = "inline"
		span.style.marginLeft = "20px"
		span.innerHTML = "Wrong username/password"
		rememberMe.appendChild(span)
		
	}
	
	
	
}


function closeModal(){
	const loginModal = document.getElementById("logInModalCenter")
	loginModal.querySelector("#clientLoginEmail").value = ""
	loginModal.querySelector("#clientLoginPassword").value = ""
	if (document.getElementById("wrongCredentials") != null){
		wrongCreds = document.getElementById("wrongCredentials")
		wrongCreds.parentNode.removeChild(wrongCreds)
		
		
	}
	
}