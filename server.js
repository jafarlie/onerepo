//Defing Console Log
const log = console.log();
const print = console.log;

//Built in Modules
const path = require('path');

//Express
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');

const hbs = require('hbs');
const { MongoClient, ObjectID } = require('mongodb');
const mongoose = require('./src/db/mongoose.js');
const { User } = require('./src/models/user');

const bcrypt = require('bcryptjs')
//const { mongoose } = require();
// then import models
const port = process.env.PORT || 3000;
const app = express();
// body-parser middleware setup.  Will parse the JSON and convert to object
app.use(bodyParser.json());
// parse incoming parameters to req.body
app.use(bodyParser.urlencoded({ extended:true }));

// app.set('view engine', 'hbs');
// static js directory
//app.use("/js", express.static(__dirname + 'public/scripts/'));
//app.use("/css", express.static(__dirname + 'public/styles/'));
app.use(express.static(path.join(__dirname, 'public')));
// middle functions
app.use((req, res, next) => {
    console.log(`${new Date().toString()} => ${req.originalUrl}`);
    next();
});

const indexHTMLPage = path.join(__dirname + 'pages/index.html');
const userHomePage = path.join(__dirname + 'public/pages/user-home.html');
const adminDashboardPage = path.join(__dirname + '/public/pages/admin.html');
const adminUsersPage = path.join(__dirname + '/public/pages/users.html');
const privacyPage = path.join(__dirname +  'public/pages/privacy.html');
//app.use(express.static(__dirname + "../public/pages"));

app.use(session({
    secret: 'oursecret',
	resave: false,
	saveUninitialized: false,
	cookie: {
		expires: 600000,
		httpOnly: true
	}
}));

// Add middleware to check for logged-in users
const sessionChecker = (req, res, next) => {
	if (req.session.user) {
		res.redirect('dashboard');
	} else {
		next();
	}
};
// Add middleware to check for logged-in admin users
const adminSessionChecker = (req, res, next) => {
    if(req.session.user){
        res.redirect('adminDashboard');
    }else{
        next();
    }
};

// Middleware for authentication for resources
const authenticate = (req, res, next) => {
	if (req.session.user) {
		User.findById(req.session.user).then((user) => {
			if (!user) {
				return Promise.reject();
			} else {
				req.user = user;
				next();
			}
		}).catch((error) => {
			res.redirect('/');
		});
	} else {
		res.redirect('/');
	}
};

const fileRouter = require('./routers/standalone-file.js');
const directoryRouter = require('./routers/standalone-directory.js');
app.use(fileRouter);
app.use(directoryRouter);

const authenticateAdmin = (req, res, next) => {
    if(req.session.user){
        User.findById(req.session.user).then((user) => {
            if(!user){
                return Promise.reject();
            }
            else if(!user.isAdmin){
                return Promise.reject();
            }else{
                req.user = user;
				next();
            }
        }).catch((error) => {
            res.redirect('/admin');
        });
    }else{
        res.redirect('/admin');    
    }
};

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, "/public/pages/index.html"));
});

app.get('/signInError', (req, res) => {
    res.sendFile(__dirname + '/public/pages/fakeIndex.html');
});

// routes for signin and sign-out
app.post('/login', (req, res) => {
    //log(req.body.isAdmin);
    const email = req.body.email;
    const password = req.body.password;
    User.findByEmailPassword(email, password).then((user)=>{
        if(!user){
            res.redirect('/signInError');
        }else{
            req.session.user = user._id;
            req.session.email = user.email;
            //res.send(user);
            res.redirect('/dashboard');
        }
    }).catch((error) => {
        res.redirect('/signInError');
    });
});
/*
app.route('/login').get(sessionChecker, (req, res) => {
		res.sendFile(__dirname + "/public/pages/index.html");
}); */

app.post('/signup', (req, res)=> {
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        modifiedDate: new Date()
    });
    User.find({email: req.body.email}).then((result) => {
        if(result.length === 0){
            //res.send(result);
            
            user.save().then((result) => {
                req.session.user = user._id;
                req.session.email = user.email;
                res.sendFile(path.join(__dirname, "/public/pages/user-home.html"));
            }).catch((error) => {
                //res.sendFile(path.join(__dirname, "/public/pages/index.html"));

                res.send(error.message);
            }); 
        }else{
            res.redirect('/');
        }
    });
});

app.get('/dashboard', (req, res) => {
	// check if we have active session cookie
	if (req.session.user) {
		res.sendFile(__dirname + '/public/pages/user-home.html');
		//res.render('dashboard.hbs', {
		//	email: req.session.email
		//});
	} else {
		res.redirect('/login');
	}
});


app.get('/user', (req, res)=> {
    res.sendFile(path.join(__dirname, "/public/pages/user-home.html"));
});

// route for logout    
app.get('/logout', (req, res) => {
    
    req.session.destroy((error) => {
        if (error) {
            res.status(500).send(error);
        } else {
            res.redirect('/');
        }
    });  
    //res.redirect('/');
});


app.post('/verifyPassword/:id', (req, res) =>{

    const id = req.params.id
    const password = req.body.password

    User.findById(id).then((user) => {
		if (!user) {
			res.send(false)
		}

	
			bcrypt.compare(password, user.password, (error, result) => {
                if(error){
                    res.send(false)
                }
				res.send(true)
			});
		
	});




});

// ========routes for admin user ==========
app.get('/admin', adminSessionChecker, (req, res) => {
    res.sendFile(path.join(__dirname + '/public/pages/admin-signin.html'));
});

app.post('/adminsign', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    User.findAdminByEmailPassword(email, password).then((user)=>{
        if(!user){
            res.sendFile(path.join(__dirname, "/public/pages/admin-signin.html"));
        }else{
            req.session.user = user._id;
            req.session.email = user.email;
            req.session.isAdmin = user.isAdmin;
            //res.send(user);
            res.redirect('/adminDashboard');
        }
    }).catch((error) => {
        res.redirect('/admin');
        //res.sendFile(path.join(__dirname, "/public/pages/index.html"));
    });
});

app.get('/adminUserCount', (req, res) => {
    User.find({isAdmin: false}).then((users) => {
        res.send(users);
    }, (error) => {
        res.status(500).send(error);
    });
});

app.get('/adminLatest3Users', (req, res) => {
    User.find({isAdmin: false}).then((users) => {
        users.sort(function(a,b){
            return new Date(b.modifiedDate)- new Date(a.modifiedDate);
        });
        res.send(users);
    }, (error) => {
        res.status(500).send(error);
    });
});
// In case if we want to create new admin user
/*
app.post('/adminsignup', (req, res)=>{
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        isAdmin: true
    });
    user.save().then((result) => {
        res.send(user);
    }).catch((error) => {
        //res.sendFile(path.join(__dirname, "/public/pages/index.html"));
        res.send("Admin create Error");
    });
}); */

app.get('/adminDashboard', (req, res) => {
    if (req.session.user) {
		res.sendFile(__dirname + '/public/pages/admin.html');
		//res.render('dashboard.hbs', {
		//	email: req.session.email
		//});
	} else {
		res.redirect('/admin');
	}
});
app.get('/adminlogout', (req,res) => {
    req.session.destroy((error) => {
        if (error) {
            res.status(500).send(error);
        } else {
            res.redirect('/admin');
        }
    }); 
});

app.get('/getUsersForAdmin', authenticateAdmin, (req, res) => {
    //isAdmin: false
    User.find({isAdmin: false}).then((users) => {
        res.sendFile(adminUsersPage);
    }, (error) => {
        res.status(500).send(error);
    });
});
app.get('/usersForAdmin', authenticateAdmin, (req, res) => {
    //isAdmin: false
    User.find({isAdmin: false}).then((users) => {
        res.send({users});
    }, (error) => {
        res.status(500).send(error);
    });
});
app.get('/getUsers',authenticate, (req, res) => {

});

app.post('/adminCreateUser', (req, res) => {
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        modifiedDate: new Date()
    });

    User.find({email: req.body.email}).then((result) => {
        if(result.length === 0){
            //res.send(result);
            
            user.save().then((result) => {
                res.sendFile(adminUsersPage);
            }).catch((error) => {
                //res.sendFile(path.join(__dirname, "/public/pages/index.html"));
                res.send(error.message);
            });
        }else{
            res.redirect('/getUsersForAdmin');
        }
    });
});


app.post('/adminDeleteUser', (req, res) => {
    User.findOneAndRemove({_id: new ObjectID(req.body.userId)}, (err, offer) => {
        if(err){
            res.send('Error occured, can\'t delete');
        }else{
            res.redirect('/getUsersForAdmin');
        }
    });
});

app.post('/adminEditUser', (req, res) => {
    User.findById({_id: new ObjectID(req.body.editUserId)}).then((result) => {
        const userName = req.body.editUserName;
        const userEmail = req.body.editUserEmail;
        if(userName.length === 0 && userEmail.length === 0){
            res.redirect('/getUsersForAdmin');
        }else if(userName.length === 0 && userEmail.length !== 0){
            result.email = userEmail;
            result.save();
            res.redirect('/getUsersForAdmin');
        }else if(userName.length !== 0 && userEmail.length === 0){
            result.name = userName;
            result.save();
            res.redirect('/getUsersForAdmin');
        }else{
            result.name = userName;
            result.email = userEmail;
            result.save();
            res.redirect('/getUsersForAdmin');
        }
        
    }).catch((error) => {
        res.status(500).send(error);
    });
});

app.post('/user/home/:id/authenticateCloudAcc', (req, res) => {

});
// ============routes for usual user ===============
app.get('/user/files/', (req, res) => {

});

app.post('/user/files/', (req, res) => {

});

app.get('/user/profile/', (req, res) => {
    console.log(req.session.user)
    res.sendFile(path.join(__dirname, '/public/pages/profile.html'));
});

app.get('/user/home/', (req, res) => {

});

// expects body {"full_name", "email", "password", "delete_account"}
app.get('/currentUserInfo', (req, res) =>{
    User.findById({_id: new ObjectID(req.session.user)}).then((user) =>{

        if(user){

            res.send(user);

        }
    });


});

app.post('/user/profile/:id', (req, res) => {

    const id = req.params.id;
    const newPass = req.body.password;

    // grab the properties we want to change
    

    User.findById({_id: new ObjectID(id)}).then((user)=>{

        user.password = newPass
        user.save().then((result)=>{


            res.send(result)

        }).catch((error)=>{

            console.log(error)
            res.status(500).send(error)

        })



    })
    // Good practise: Validate the id
    


});

app.post('/userDisconnectDrive/:id', (req, res) => {

    const id = req.params.id;
    const drive = req.body.drive;

    // grab the properties we want to change
    

    User.findById({_id: new ObjectID(id)}).then((user)=>{

        if(drive == 'google'){

            user.googleDriveID = null
        }
        else{
            user.oneDriveID = null
        }
        user.save().then((result)=>{


            res.send(result)

        }).catch((error)=>{

            console.log(error)
            res.status(500).send(error)

        })



    })
    // Good practise: Validate the id
    


});

// Handler for error-404 --- Response not found
app.use((req,res,next) => {
    res.status(404).send("We think ur lost");
});


// Handler for error 500
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.sendFile(path.join(__dirname, '/public/pages/500.html'));
});


app.listen(port, () => console.info(`Server is running on ${port}`));
